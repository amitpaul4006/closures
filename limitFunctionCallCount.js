function limitFunctionCallCount(callback, n) {
    let val = 1
    function calling() {
        if (val <= n) {
            val += 1
            return callback()
        }
        else {
            return null
        }

    }
    return calling
}
module.exports = limitFunctionCallCount;