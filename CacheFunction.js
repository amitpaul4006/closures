function Cache(callBack) {
    let cache_obj = {};
    function calling(n) {
        if (n in cache_obj) {
            return cache_obj
        }
        else {
            cache_obj[n] = n
            return callBack(n)

        }

    }
    return calling
}
module.exports = Cache