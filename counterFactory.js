function counterFactory() {
    let counter = 3;
    let object = {
        increment: function () {
            return counter += 1;
        },
        decrement: function () {
            return counter -= 1;
        }
    }
    return (object)
}
module.exports = counterFactory;